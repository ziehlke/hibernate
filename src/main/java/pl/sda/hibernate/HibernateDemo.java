package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.hibernate.domain.Employee;
import pl.sda.hibernate.util.HibernateUtil;

public class HibernateDemo {

    public static void main(String[] args) {

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;

        try {
            sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

//            for (int i = 0; i < 1000; i++) {
                Employee employee = session.get(Employee.class, 1);
                System.out.println(employee);
//            }


            Employee employee1 = new Employee("Jan", "Kowalski", 25);
//            session.save(employee1);


            employee.setAge(100);
            employee.setPassword("#$@Sdfsttsdf$#@");

            session.delete(employee);


            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
            if (sessionFactory != null) {
                sessionFactory.close();
            }
        }
    }
}
